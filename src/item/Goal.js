import Item from './Item'

export default class Goal extends Item {
  constructor(o) {
  	super(o);
    this.id = 'Goal-' + ("" + Math.random()).substring(12);
    this.z = 10;
    this.w = 80;
    this.h = 20;
    this.d = 20;
   	if (this.team == Item.TEAM_A)
      this.angle = Math.PI
  }
	object3D(){
		const o = new THREE.Mesh(new THREE.BoxGeometry(this.w,this.h,this.d,2,2,2),new THREE.MeshBasicMaterial({
			color : this.color(),
			wireframe : true
		}) );
		return o;
	}
	positionForGoalkeeper(){
		return {x:this.x, y: this.y + (50*(this.angle?-1:1))}
	}
}

class ModelledFootballGoal extends Goal {
	object3D(){
		const loader = new THREE.OBJLoader();
		const o = new THREE.Object3D();
		const self = this;
		loader.load( window.staticPrefix+'/pub/obj/goal/postline_v03.obj', function ( object ) {
			//const material = new THREE.MeshBasicMaterial({color: self.color() });
			const material = new THREE.MeshLambertMaterial( { color: self.color(), shading: THREE.SmoothShading } )
			//const material = new THREE.MeshLambertMaterial({color: self.color() });
			object.traverse(function ( child )	{
				if ( child instanceof THREE.Mesh ) {
					child.geometry.computeVertexNormals();
					child.material = material;
				}
			});
			// TODO save angle of the goal to self.angle for other sports which need to know if
			// Player or Ball is on shootable side of the goal
			o.rotation.x = Math.PI/2;
			o.rotation.y = self.angle;
			o.scale.set(12,12,12);
			o.needsUpdate = true;
			object.needsUpdate = true;
			o.add(object);
			setTimeout(function(){
				if( window.tacmap )
					window.tacmap.update();
			},0);
		});
		/*const map = THREE.ImageUtils.loadTexture( '/pub/obj/goal/net.bmp' );
		 loader.load( '/pub/obj/goal/net_v01.obj', function ( object ) {
		 const material = new THREE.MeshLambertMaterial( { map: map, transparent: true } );
		 material.opacity = 0.95;
		 object.traverse(function ( child )	{
		 if ( child instanceof THREE.Mesh ) {
		 child.material = material;
		 //child.material.color.setHex(0xFFFFFF);
		 }
		 });
		 o.scale.set(115,115,115);
		 o.needsUpdate = true;
		 object.needsUpdate = true;
		 o.add(object);
		 });*/
		return o;
	}
}