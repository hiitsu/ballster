import Item from './Item'

class Field {
	constructor(o){
		this.id = 'Field-'+(""+Math.random()).substring(12);
		this.w = 500;
		this.h = 1000;
	}
	positionForBall(){
		return { x :0, y:0 };
	}
	positionForPlayer(team,nth,playersPerRow){
		var spacing = 50;
		playersPerRow = playersPerRow || 5;
		var offsetX = -spacing * (playersPerRow/2) +(spacing/2);
		var rows = nth < playersPerRow ? 1 : 2;
		var offsetY = team != Item.TEAM_A ? -spacing*3*rows : spacing*3*rows;
		return { x:offsetX+(spacing*((nth+1)%playersPerRow)),y: offsetY };
	}
	positionForGoal(goal){
		var y = this.y-this.h/2-goal.h/2;
		if( goal.team != Item.TEAM_B ) {
			y = this.y+this.h/2+goal.h/2;
		}
		goal.x = 0;
		goal.y = y;
	}
	/**
	 * Add lines to given mesh plane with given z coordinate
	 * @param planeMesh	Pitch plane mesh
	 * @param z			Height of the lines
	 */
	lines(planeMesh,z,color){
		z = z || 1.5;
		var opacity = 0.8;
		var centerLineGeometry = new THREE.Geometry();
		centerLineGeometry.vertices.push(new THREE.Vector3(-250, 0, z));
		centerLineGeometry.vertices.push(new THREE.Vector3(250, 0, z));
		var centerLineMaterial = new THREE.LineBasicMaterial( { color: color || 0xffffff,transparent:true,opacity:opacity, linewidth: 1 } );
		var centerLine = new THREE.Line(centerLineGeometry, centerLineMaterial);
		planeMesh.add(centerLine);

		{
			var sideLineGeometry = new THREE.Geometry();
			sideLineGeometry.vertices.push(new THREE.Vector3(-250, 500, z));
			sideLineGeometry.vertices.push(new THREE.Vector3(250, 500, z));
			sideLineGeometry.vertices.push(new THREE.Vector3(250, -500, z));
			sideLineGeometry.vertices.push(new THREE.Vector3(-250, -500, z));
			sideLineGeometry.vertices.push(new THREE.Vector3(-250, 500, z));
			var sideLineMaterial = new THREE.LineBasicMaterial( { color: color || 0xffffff,transparent:true,opacity:opacity, linewidth: 1 } );
			var sideLineMesh = new THREE.Line(sideLineGeometry, sideLineMaterial);
			planeMesh.add(sideLineMesh);
		}
		var centerCircleMaterial = new THREE.LineBasicMaterial( { color: color || 0xffffff,transparent:true,opacity:opacity, linewidth: 1 } );
		var radius = 100;
		var segments = 128;
		var centerCircleGeometry = new THREE.CircleGeometry( radius, segments );
		var centerCircleMesh = new THREE.Line( centerCircleGeometry, centerCircleMaterial );
		centerCircleMesh.position.z = z;
		planeMesh.add( centerCircleMesh );

		var rectLength = 170/2, rectWidth = 320/2;
		 _.each([500,-500],function(y){
			 var penaltyBoxGeometry = new THREE.Geometry();
			 var penaltyBoxDirection = (y<0?rectLength:-rectLength);
			 penaltyBoxGeometry.vertices.push(new THREE.Vector3(-rectWidth, y, z));
			 penaltyBoxGeometry.vertices.push(new THREE.Vector3(-rectWidth, y+penaltyBoxDirection, z));
			 penaltyBoxGeometry.vertices.push(new THREE.Vector3(rectWidth, y+penaltyBoxDirection, z));
			 penaltyBoxGeometry.vertices.push(new THREE.Vector3(rectWidth, y, z));
			 var penaltyBoxMaterial = new THREE.LineBasicMaterial( { color: color || 0xffffff,transparent:true,opacity:opacity, linewidth: 1 } );
			 var penaltyBox = new THREE.Line(penaltyBoxGeometry, penaltyBoxMaterial);
			 planeMesh.add(penaltyBox);
		 });
	}
	object3D(){
		var planeGeometry = new THREE.PlaneGeometry( this.w, this.h );
		var planeMaterial = new THREE.MeshBasicMaterial ( {color: 0x00aa00, side: THREE.DoubleSide} );
		var planeMesh = new THREE.Mesh( planeGeometry, planeMaterial );
		this.lines(planeMesh);
		return planeMesh;
	}
}


class FancyField extends Field {
	object3D(){
		var textureGrass = tiledTexture(window.staticPrefix+'/pub/img/grass.jpg', 32, 32);
		var grass = box(this.w+80, this.h+80, 2, {
			ambient		: 0x444444,
			color		: 0xffffff,
			shininess	: 1,
			specular	: 0x33AA33,
			shading		: THREE.SmoothShading,
			map         : textureGrass,
			wrapS		: THREE.RepeatWrapping,
			wrapT		: THREE.RepeatWrapping
		});
		this.lines(grass);
		grass.position.set(0,0,-5);
		return grass;
	}
}


class StripedField extends Field {
	object3D(){
		var planeGeometry = new THREE.PlaneGeometry( this.w+60, this.h+60 );
		var planeMaterial = new THREE.MeshBasicMaterial ( {color: 0x5C9D1B, side: THREE.DoubleSide} );
		var planeMesh = new THREE.Mesh( planeGeometry, planeMaterial );
		this.lines(planeMesh,null,0xBAD380);
		return planeMesh;
	}
}