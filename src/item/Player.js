import Item from './Item'

export default class Player extends Item {

	constructor(o){
		super(o);
		this.id = 'Player-'+(""+Math.random()).substring(12);
		this.z = 15;
		this.numberBadge = '12';
	}

	movable() {
	  return true;
  }

	selectable() {
	  return true;
  }

	blocks(fromX,fromY,toX,toY){
		const p = closestPointInLine(fromX,fromY,toX,toY,this.x,this.y);
		return (p.distance < Player.REACH_DISTANCE);
	}

	object3D(){

		// wrapper composite object for switching between flat circle object and 3d box
		const wrapperObject = new THREE.Object3D();
		wrapperObject.name = this.id;

		const label = createCirleLabel(this.numberBadge ? this.numberBadge : ' ', 0, 0, 0, 96, 'white', this.colorText());
		label.scale.set(0.2,0.2,0.2);
		label.visible = false;
		wrapperObject.add(label);

		const boxMaterial = new THREE.MeshLambertMaterial({color : this.color(),  shading: THREE.FlatShading});
		const boxMesh = new THREE.Mesh(new THREE.BoxGeometry(12,12,30,1,1,1),boxMaterial);
		boxMesh.geometry.computeFaceNormals();
		boxMesh.geometry.computeVertexNormals();
		wrapperObject.add(boxMesh);

		const textSprite = new THREE.TextSprite(0,0,32);
		textSprite.setText(this.numberBadge ? this.numberBadge : ' ',this.colorText(),256);
		textSprite.scale.set(24,24,0);
		boxMesh.add(textSprite);

		/*const circleMaterial = new THREE.MeshBasicMaterial( { opacity:1, transparent:false,color: this.color() } )
			,circleGeometry = new THREE.CircleGeometry( 10, 10 )
			,circleMesh = new THREE.Mesh( circleGeometry, circleMaterial );
		circleMesh.position.z = 1.5;
		wrapperObject.add(circleMesh);*/

		wrapperObject.addEventListener('cameraChange',function(ev){
			wrapperObject.needsUpdate = label.needsUpdate = true;
			if( /(stadium|goaltender)/i.test(ev.cameraType) ) {
				label.visible = false;
				boxMesh.visible = true;
			} else {
				boxMesh.visible = false;
				label.visible = true;
			}

		});
		return wrapperObject;
	}
}

Player.REACH_DISTANCE = 20;
