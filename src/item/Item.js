/**
 * Base class for all team and tactic related items that affect passing lines, shooting sectors,
 * ball movement and which may have interaction, this includes fields, balls, players, goals, nets, and so on.
 *
 * All purely visual that vary from during animation of editpoint should go to BaseScene which can recycle them.
 *
 * @param o      Optional property map for copy construction or cloning etc purposes
 * @constructor
 */
class Item {
  constructor(o) {
    if (!this.id)
      this.id = `Item-${Date.now()}`;
    this.team = Item.NO_TEAM;
    this.x = this.x || 0;
    this.y = this.y || 0;
    this.z = this.z || 0;
    this.angle = this.angle || 0;
    if (o) _.extend(this, o);
  }

  movable() {
    return false;
  }

  selectable() {
    return false;
  }

  color() {
    return (this.team == Item.NO_TEAM ? 0x333333 : (this.team == Item.TEAM_A ? 0xFEC903 : 0x6ABADD));
  }

  colorText() {
    return `#${(this.team == Item.NO_TEAM ? 0x333333 : (this.team == Item.TEAM_A ? 0xFEC903 : 0x6ABADD)).toString(16)}`;
  }

  update(scene) {
    console.warn('calling Item.update');
    if (!scene.getObjectByName(this.id)) {
    }
  }

  blocks(fromX, fromY, toX, toY) {
    return false;
  }

  contains(x, y) {
    return Math.sqrt((this.x - x) ** 2 + (this.y - y) ** 2) < 20;
  }
}

Item.TEAM_A = 1;
Item.TEAM_B = 2;
Item.NO_TEAM = 0;