export default class ItemManager {
  constructor() {
    this.items = [];
  }

  clear() {
    this.items = [];
  }

  itemCount() {
    return this.items.length;
  }

  add(item) {
    this.items.push(item);
  };

  deleteItemById(id) {
    this.items = _.without(this.items, _.findWhere(this.items, {id}));
  }

  item(index) {
    return this.items[index];
  }

  itemById(id) {
    return _.findWhere(this.items, {id});
  }

  itemCloseBy(x, y) {
    for (const o of this.items) {
      const d = Math.sqrt((o.x - x) ** 2 + (o.y - y) ** 2);
      //console.log(o.id +' is at distance '+d+' from '+parseInt(x)+','+parseInt(y));
      if (d < 40) {
        console.log(`hit item ${o.id}`);
        return o;
      }
    }

    return null;
  }

  selectMovableItemCloseByXY(x, y, excludeBall) {
    const items = !excludeBall ? this.items : _.filter(this.items, item => !(item instanceof Ball), this);
    const sortedByDistance = _.sortBy(items, o => {
      if( !o.selectable() || !o.movable() )
      return Number.MAX_VALUE;
      return o.d = Math.sqrt((o.x - x) ** 2 + (o.y - y) ** 2);
    });
    if (sortedByDistance.length && sortedByDistance[0].d < 50)
      return sortedByDistance[0];
    return null;
    /*for(var i=0; i < this.items.length;i++) {
     var o = this.items[i];
     if( !o.selectable() || !o.movable() )
     continue;
     var d = Math.sqrt(Math.pow(o.x-x,2) + Math.pow(o.y-y,2));
     //console.log(o.id +' is at distance '+d+' from '+parseInt(x)+','+parseInt(y));
     if( d < 40 ) {
     console.log('selecting item '+o.id);
     return o;
     }
     }
     return null;*/
  }

  ball() {
    return this.itemByType(Ball);
  }

  itemByType(klass) {
    for (let i = 0; i < this.items.length; i++) {
      if (this.items[i] instanceof klass)
        return this.items[i];
    }
    return null;
  }

  isPlayer(id) {
    return this.itemById(id) instanceof Player;
  }

  itemsByType(klass) {
    const a = [];
    for (let i = 0; i < this.items.length; i++) {
      if (this.items[i] instanceof klass)
        a.push(this.items[i]);
    }
    return a;
  }

  players(team) {
    const a = this.itemsByType(Player);
    return _.filter(a, p => p.team == team);
  }

  opposingGoal(playedOrId) {
    const player = this.itemById(playedOrId);
    const goals = this.itemsByType(Goal);
    return _.find(goals, function (g) {
      return g.team != this.team;
    }, player);
  }

  hasBall(idOrPlayer) {
    const player = idOrPlayer instanceof Player ? idOrPlayer : this.itemById(idOrPlayer);
    const p = this.playerWithBall();
    return (p && player && p.id == player.id);
  }

  /*this.canBallBeLocked = function(){
   return !!this.playerWithBall();
   };*/
  canReachTheBall(idOrPlayer) {
    const player = idOrPlayer instanceof Player ? idOrPlayer : this.itemById(idOrPlayer);
    const ball = this.itemByType(Ball);
    if (!ball || !player || player instanceof Ball)
      return false;
    return Math.sqrt((player.x - ball.x) ** 2 + (player.y - ball.y) ** 2) <= Player.REACH_DISTANCE;
  }

  playerWithBall() {
    const ball = this.itemByType(Ball);
    const players = this.itemsByType(Player);
    if (!ball || !players || !players.length)
      return null;
    let shortestDistance = Number.MAX_VALUE;
    let closestPlayer = null;

    for (const p of players) {
      const d = Math.sqrt((p.x - ball.x) ** 2 + (p.y - ball.y) ** 2);
      if (d < shortestDistance) {
        shortestDistance = d;
        closestPlayer = p;
      }
    }

    if (shortestDistance <= Player.REACH_DISTANCE)
      return closestPlayer;
    return null;
  };

  teamMates(id) {
    const p = this.itemById(id);
    const teamMates = _.filter(this.players(p.team), p => p.id != id)
    ;
    return teamMates;
  }

  opponents(id) {
    const p = this.itemById(id);
    const opponents = _.filter(this.itemsByType(Player), item => p.team != item.team)
    ;
    return opponents;
  }

  passingLanesForPlayer(id) {
    const p = this.itemById(id);
    const teamMates = this.teamMates(id);
    const a = [];
    const opponents = this.opponents(id);
    _.each(teamMates, function (mate) {
      const blocked = _.find(opponents, opponent => opponent.blocks(p.x, p.y, mate.x, mate.y));
      const tooFar = lineLength(p.x, p.y, mate.x, mate.y) > 750; // 75m
      if (!tooFar && !blocked)
        this.push([new THREE.Vector3(p.x, p.y, 50), new THREE.Vector3(mate.x, mate.y, 50)])
    }, a);
    return a;
  }

  shootingSectorForPlayer(id) {
    const p = this.itemById(id);
    const goal = this.opposingGoal(id);
    const opponents = this.opponents(id);
    const blocked = _.find(opponents, opponent => {
      const blocks = opponent.blocks(p.x, p.y, goal.x, goal.y);
    const isClose = lineLength(opponent.x, opponent.y, p.x, p.y) < 70;
    return isClose && blocks;
  })
    ;
    if (blocked)
      return null;
    const distance = Math.sqrt((goal.y - p.y) ** 2 + (goal.x - p.x) ** 2);
    const isTooFar = distance > 350; // 35m
    if (isTooFar)
      return null;

    const //[new THREE.Vector3(p.x, p.y, 50),new THREE.Vector3(goal.x+goal.width/2, goal.y,50),new THREE.Vector3(goal.x-goal.width/2, goal.y,50)]
      theta = new THREE.Vector3(p.x - goal.x - goal.w / 2, p.y - goal.y, 0).angleTo(new THREE.Vector3(p.x - goal.x + goal.w / 2, p.y - goal.y, 0));

    const angle = Math.atan2((goal.y - p.y), (goal.x - p.x)) - theta / 2;
    const sector = {x: p.x, y: p.y, radius: distance, theta, angle};
    //console.log(sector);
    return sector;
  }

  rotateTo(id, angle) {
    const item = _.findWhere(this.items, {id});
    item.angle = angle;
  }

  moveTo(id, x, y) {
    const item = _.findWhere(this.items, {id});
    //console.log('moveTo '+item.id+' to '+x+','+y);
    item.x = x;
    item.y = y;
  }

  addSceneObjects(scene) {
    _.each(this.items, function (item) {
      let o = this.getObjectByName(item.id);
      if (!o) {
        o = item.object3D();
        o.name = item.id;
        this.add(o);
      }
      o.position.set(item.x, item.y, item.z);
    }, scene);
  }

  update(scene) {
    this.addSceneObjects(scene);
  }
}
