import Item from './Item'

class Ball extends Item {
	constructor(o){
		super(o);
		this.id = 'Ball-'+(""+Math.random()).substring(12);
		this.z = 7;
	}
	movable() { return true; }
	selectable() { return true; }
	object3D(){
		var o = new THREE.Mesh(new THREE.SphereGeometry(8,8,8),new THREE.MeshLambertMaterial({ color : 0xefefef}) );
		return o;
	}
}

class FancyBall extends Ball {
	constructor(o){
		super(o);
		this.id = 'FancyBall-'+(""+Math.random()).substring(12);
	}
	object3D(){
		var radius = 10,
			segments = 6,
			rings = 6;
		var sphereMaterial = new THREE.MeshLambertMaterial({
			color: 0xFFFFFF,
			ambient: 0xFFFFFF,
			emissive: 0x000000,
			map: THREE.ImageUtils.loadTexture(window.staticPrefix+'/pub/img/football.jpg',THREE.SphericalReflectionMapping,function(){
				setTimeout(function(){
					if( window.tacmap )
						window.tacmap.update();
				},0);
			})
		});
		var ball = new THREE.Mesh(new THREE.SphereGeometry(radius,segments,rings),sphereMaterial);
		return ball;
	}
}
