(function() {
	var stateModel;
	module("StateModel",{
		setup:function(){
			stateModel = new StateModel();
		}
	});
	test("selectedClass",function() {
		ok( !stateModel.selectedClass() );
		stateModel.set('selected',[new Player]);
		equal(stateModel.selectedClass(),'Player');
	});

})();

(function() {
	var offCanvasManager;
	module("OffCanvasManager",{
		setup:function(){
			offCanvasManager = new OffCanvasManager();
		}
	});
	test("getTextureForText",function() {
		var c1 = offCanvasManager.getTextureForText('123');
		var c2 = offCanvasManager.getTextureForText('123');
		var c3 = offCanvasManager.getTextureForText('123');
		equal(c1,c2);
		equal(c2,c3);
		equal(c1.width,c2.width);
		equal(c2.height,c3.height);
		equal(_.size(offCanvasManager.canvasMap),1);
		var c4 = offCanvasManager.getTextureForText('123','green');
		equal(_.size(offCanvasManager.canvasMap),2);
		var c5 = offCanvasManager.getTextureForText('abc','#00ff00');
		var c6 = offCanvasManager.getTextureForText('abc','rgb(128,128,128)',100);
		equal(_.size(offCanvasManager.canvasMap),4);
		equal(c6.height,100);
		window.open(c1.toDataURL('image/png'),'_blank');
		window.open(c5.toDataURL('image/png'),'_blank');
		window.open(c6.toDataURL('image/png'),'_blank');
	});

})();

(function() {
	var item;
	module("Item",{
		setup:function(){
			item = new Item();
		}
	});
	test("instanceof",function() {
		ok(item instanceof Item);
	});

	test("copy constructor",function() {
		var item2 = new Item(item);
		equal(item.id,item2.id);
		var item3 = new Item({x:1,y:2});
		var item4 = new Item(item3);
		equal(item3.id,item4.id);
		equal(item3.x,item4.x);
		equal(item3.y,item4.y);
	});
}());

(function() {

	var itemManager, player,playerB, goal,goalB, field,ball;
	module("ItemManager",{
		setup:function(){
			itemManager = new ItemManager();
			player = new Player({team:Item.TEAM_A});
			playerB = new Player({team:Item.TEAM_B});
			goal = new Goal({team:Item.TEAM_A});
			goalB = new Goal({team:Item.TEAM_B});
			field = new Field();
			ball = new Ball();
		}
	});

	test("deleteItemBy",function() {
		itemManager.add(player);
		itemManager.add(goal);
		itemManager.add(field);
		itemManager.deleteItemById(player.id);
		equal(0,itemManager.itemsByType(Player).length);
		equal(2,itemManager.itemCount());
	});
	test("itemByType",function() {
		itemManager.add(player);
		itemManager.add(goal);
		itemManager.add(field);
		equal(player.id,itemManager.itemByType(Player).id);
		equal(goal.id,itemManager.itemByType(Goal).id);
		equal(field.id,itemManager.itemByType(Field).id);
	});
	test("itemsByType",function() {
		itemManager.add(player);
		itemManager.add(field);
		itemManager.add(new Player());
		equal(2,itemManager.itemsByType(Player).length);
		equal(1,itemManager.itemsByType(Field).length);
	});
	test("opposingGoal",function() {
		itemManager.add(player);
		itemManager.add(field);
		itemManager.add(playerB);
		itemManager.add(goal);
		itemManager.add(goalB);
		equal(goalB.id,itemManager.opposingGoal(player.id).id);
		equal(goal.id,itemManager.opposingGoal(playerB.id).id);
	});

	test("players",function() {
		itemManager.add(player);
		itemManager.add(new Player({team:Item.TEAM_A}));
		equal(2,itemManager.players(Item.TEAM_A).length);
		equal(0,itemManager.players(123).length);
		itemManager.add(new Player({team:Item.TEAM_B}));
		itemManager.add(new Player({team:Item.TEAM_B}));
		equal(2,itemManager.players(Item.TEAM_B).length);
	});

	test("playerWithBall & hasBall & canReachTheBall",function() {
		itemManager.add(new Player({id:'a',x:10,y:10,team:Item.TEAM_A}));
		var playerB = new Player({id:'b',x:30,y:30,team:Item.TEAM_A});
		itemManager.add(playerB);
		itemManager.add(new Player({id:'c',x:50,y:50,team:Item.TEAM_A}));
		var b = new Ball({x:35,y:35});
		itemManager.add(b);
		ok(itemManager.canReachTheBall('b'));
		ok(itemManager.canReachTheBall(playerB));
		ok(!itemManager.canReachTheBall('a'));
		equal(itemManager.playerWithBall().id,'b');
		equal( itemManager.hasBall('a'),false );
		equal( itemManager.hasBall('b'),true );
		equal( itemManager.hasBall('c'),false );
		b.x = b.y = 11;
		equal(itemManager.playerWithBall(),itemManager.items[0]);
		ok( itemManager.hasBall('a'))
		itemManager.deleteItemById(b.id);
		equal(itemManager.playerWithBall(),null);
		equal( !!itemManager.hasBall('a'),false );
		equal( !!itemManager.hasBall('b'),false );
		equal( !!itemManager.hasBall('c'),false );
	});
	test("teamMates",function() {
		itemManager.add(new Player({x:10,y:10,team:Item.TEAM_A,id:1}));
		itemManager.add(new Player({x:30,y:30,team:Item.TEAM_A}));
		itemManager.add(new Player({x:50,y:50,team:Item.TEAM_A}));
		equal(itemManager.teamMates(1).length,2);
	});
	test("opponents",function() {
		itemManager.add(new Player({x:10,y:10,team:Item.TEAM_A,id:1}));
		equal(itemManager.opponents(1).length,0);
		itemManager.add(new Player({x:30,y:30,team:Item.TEAM_B}));
		itemManager.add(new Player({x:50,y:50,team:Item.TEAM_A}));
		equal(itemManager.opponents(1).length,1);
		itemManager.add(new Player({x:30,y:30,team:Item.TEAM_B}));
		equal(itemManager.opponents(1).length,2);
	});
	test("passingLanesForPlayer",function() {
		itemManager.add(new Player({x:10,y:10,team:Item.TEAM_A,id:1}));
		itemManager.add(new Player({x:200,y:10,team:Item.TEAM_A}));
		itemManager.add(new Player({x:150,y:150,team:Item.TEAM_A}));
		equal(itemManager.passingLanesForPlayer(1).length,2);
		itemManager.add(new Player({x:50,y:51,team:Item.TEAM_B}));
		equal(itemManager.passingLanesForPlayer(1).length,1);
		itemManager.add(new Player({x:100,y:12,team:Item.TEAM_B}));
		equal(itemManager.passingLanesForPlayer(1).length,0);
	});

})();

(function() {
	var ball,fancyBall;
	module("Ball,FancyBall", {
		setup: function () {
			ball = new Ball();
			fancyBall = new FancyBall();
		}
	});
	test("instanceof", function () {
		ok(ball instanceof Ball);
		ok(ball instanceof Item);
		ok(fancyBall instanceof FancyBall);
		ok(fancyBall instanceof Ball);
		ok(fancyBall instanceof Item);
	});
}());


(function() {
	var goal;
	module("Goal", {
		setup: function () {
			goal = new Goal();
		}
	});
	test("instanceof", function () {
		ok(goal instanceof Goal);
		ok(goal instanceof Item);
	});
}());

(function() {
	var player;
	module("Player", {
		setup: function () {
			player = new Player();
		}
	});
	test("instanceof", function () {
		ok(player instanceof Player);
		ok(player instanceof Item);
	});
}());

(function() {
	var field;
	module("Field",{
		setup:function(){
			field = new Field();
		}
	});
	test("instanceof",function() {
		ok(field instanceof Field);
		ok(field instanceof Item);
	});
	test("copy constructor",function() {
		var field2 = new Field(new Field({w:100,h:200,id:'abc'}));
		equal(100,field2.w);
		equal(200,field2.h);
		equal('abc',field2.id);
		var field3 = new Field(field);
		equal(field3.id,field.id);
		equal(field3.w,field.w);
		equal(field3.h,field.h);
		equal(field3.z,field.z);
		equal(field3.x,field.x);
		equal(field3.y,field.y);
	});

	test("movable,selectable",function() {
		ok( !field.movable() );
		ok( !field.selectable() );
	});
	test("positionForPlayer",function() {
		var p = field.positionForPlayer(Item.TEAM_A,0);
		ok( p );
		equal(JSON.stringify(p),JSON.stringify(field.positionForPlayer(Item.TEAM_A,0)))
		var q = field.positionForPlayer(Item.TEAM_A,1);
		ok(p.x != q.x);
	});
})();
(function() {
	var player,goal,ball,field;
	module("Player,Goal,Ball,Field",{
		setup:function(){
			field = new Field();
			player = new Player({id:1});
			goal = new Goal();
			ball = new Ball();
		}
	});
	test("JSON.stringify & JSON.parse",function() {
		equal(field.klass,JSON.parse(JSON.stringify(field)).klass);
		equal(field.id,JSON.parse(JSON.stringify(field)).id);
		equal(field.x,JSON.parse(JSON.stringify(field)).x);
		equal(field.z,JSON.parse(JSON.stringify(field)).z);
		equal(field.w,JSON.parse(JSON.stringify(field)).w);
		equal(field.h,JSON.parse(JSON.stringify(field)).h);
		equal(player.z,JSON.parse(JSON.stringify(player)).z);
		equal(player.id,JSON.parse(JSON.stringify(player)).id);
		equal(player.id,1);
	});

	test("movable,selectable,object3D",function() {
		ok( player.movable() );
		ok( player.selectable() );
		ok( player.object3D() );
		ok( ball.movable() );
		ok( ball.selectable() );
		ok( ball.object3D() );
		ok( !goal.movable() );
		ok( !goal.selectable() );
		ok( goal.object3D() );
		ok( !field.movable() );
		ok( !field.selectable() );
		ok( field.object3D() );
	});
})();

(function() {
	var dataWrapper;
	module("DataWrapper",{
		setup:function(){
			dataWrapper = new DataWrapper();
		}
		,teardown:function(){

		}
	});
	test("serialize",function() {
		var a = new DataWrapper();
		a.addItem(new Player({id:'a'}));
		a.addItem(new Player());

		a.addEditPoint('a',11,22)

		var b = new DataWrapper();
		b.setData(a.toJSON());

		deepEqual(a.toJSON(), b.toJSON());

	});
	test("addEditPoint - setEditPoint",function() {
		dataWrapper.addItem(new Player({id:'a'}));
		dataWrapper.addMovementPoint(0,'a',10,10);
		equal(dataWrapper.editPoints.at(0).get('movementPointMap').a.length,2);
		dataWrapper.addEditPoint();
		equal(dataWrapper.editPoints.length,2);
		equal(dataWrapper.editPoints.at(0).get('movementPointMap').a[1].x,10);
		equal(dataWrapper.editPoints.at(1).get('movementPointMap').a[0].y,10);
		dataWrapper.addEditPoint();
		equal(dataWrapper.editPoints.length,3);
		equal(dataWrapper.editPoints.at(2).get('movementPointMap').a[0].x,10);
		equal(dataWrapper.editPoints.at(2).get('movementPointMap').a[0].y,10);
		equal(dataWrapper.editPoints.at(2).get('movementPointMap').a.length,1);
		equal(dataWrapper.editPoints.at(1).get('movementPointMap').a.length,1);
		equal(dataWrapper.editPoints.at(0).get('movementPointMap').a.length,2);
		dataWrapper.addMovementPoint(2,'a',20,20);
		equal(dataWrapper.editPoints.at(2).get('movementPointMap').a[1].x,20);
		equal(dataWrapper.editPoints.at(2).get('movementPointMap').a[1].y,20);
		equal(dataWrapper.editPoints.at(2).get('movementPointMap').a.length,2);
		dataWrapper.addMovementPoint(0,'a',30,30);
		dataWrapper.addMovementPoint(0,'a',50,50);
		equal(dataWrapper.editPoints.at(0).get('movementPointMap').a.length,4);
		dataWrapper.addMovementPoint(2,'a',77,77);
		dataWrapper.addMovementPoint(0,'a',55,55);
		equal(dataWrapper.editPoints.at(0).get('movementPointMap').a.length,5);
		equal(dataWrapper.editPoints.at(2).get('movementPointMap').a.length,3);

		var b = new DataWrapper();
		b.setData(dataWrapper.toJSON());
		equal(b.editPoints.at(0).get('movementPointMap').a.length,5);
		equal(b.editPoints.at(2).get('movementPointMap').a.length,3);
		deepEqual(b.toJSON(),dataWrapper.toJSON());
	});
	test("toFrame(n)",function() {
		var d = new DataWrapper()
		ok(d.toFrame(0,0));
		d.addItem(new Player({id:'a'}));
		ok(d.toFrame(0,0));
		d.addMovementPoint(0,'a',10,10);
		d.addMovementPoint(0,'a',22,22);
		ok(!d.toFrame(0,0));
	});
	test("totalFrames()",function() {
		var d = new DataWrapper();
		d.addItem(new Player({id:'a'}));
		d.addMovementPoint(0,'a',10,10);
		d.addMovementPoint(0,'a',22,22);
		equal(d.totalFrames(),3);
		d.addEditPoint();
		d.addMovementPoint(1,'a',55,55);
		equal(d.totalFrames(),5);
	});
	test("resetTo",function() {
		dataWrapper.resetTo(3);
		equal(dataWrapper.itemManager.items.length,12);
		equal(dataWrapper.editPoints.length,1);
		dataWrapper.resetTo(5);
		equal(dataWrapper.itemManager.items.length,16);
		equal(dataWrapper.editPoints.length,1);
	});
	test("addMovementPoint",function() {
		var d = new DataWrapper();
		d.itemManager.add(new Player({id:1}));
		d.addMovementPoint(0,1,1,1);
		d.addMovementPoint(0,1,2,2);
		equal(d.editPoints.at(0).get('movementPointMap')[1].length,1);
		d.addEditPoint();
		d.addMovementPoint(1,1,11,11);
		equal(d.editPoints.at(1).get('movementPointMap')[1].length,2);
	});
})();

(function() {
	var tacmap;
	module("TacMap",{
		setup:function(){
			tacmap = new TacMap(document.getElementById('test-container'));
		}
		,teardown:function(){
		}
	});
	test("camera",function() {
		tacmap.stadiumView()
		tacmap.helicopterView();
		ok(true);
	});
	test("resetTo",function() {
		tacmap.resetTo(3);
		equal(tacmap.dataWrapper.itemManager.items.length,12);
		equal(tacmap.dataWrapper.editPoints.length,1);
		deepEqual(tacmap.stateModel.attributes,StateModel.prototype.defaults);
	});

	test("trendAngle",function() {
		tacmap.addInput(1,1);
		tacmap.addInput(2,2);
		tacmap.addInput(3,3);
		equal(tacmap.trendAngle(),Math.PI/4);
	});
	test("resize",function() {
		tacmap.resize();
		ok(1);
	});
	test("requestAnimFrame",function() {
		ok( tacmap.requestAnimFrame() );
	});
	test("dispose",function() {
		ok( tacmap.dispose() );
	});


})();