function lineLength(x, y, x0, y0){
	return Math.sqrt((x -= x0) * x + (y -= y0) * y);
};
function closestPointInLine(x1,y1,x2,y2,x,y){
	var dx = x2 - x1
		,dy = y2 - y1
		,d = Math.sqrt( dx*dx + dy*dy )
		,ca = dx/d // cosine
		,sa = dy/d // sine
		,mX = (-x1+x)*ca + (-y1+y)*sa;

	var result = {};
	if( mX <= 0 ) {
		result.x = x1;
		result.y = y1;
	} else if( mX >= d ){
		result.x = x2;
		result.y = y2;
	} else {
		result.x = x1 + mX*ca;
		result.y = y1 + mX*sa;
	}
	dx = x - result.x;
	dy = y - result.y;
	result.distance = Math.sqrt( dx*dx + dy*dy );
	return result;
}