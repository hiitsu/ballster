/**
 * Helper to create 2d bitmaps to be mapped into 3d space.
 * @constructor
 */
function OffCanvasManager(){
	this.canvasMap = {};
	this.getTextureForText = function(s,color,size){
		var fontColor = color || 'red';
		var fontSize = size ? size-10 : 40;
		var canvasKey = s+color+size;
		if( !this.canvasMap[canvasKey] ) {
			var canvas = document.createElement('canvas')
				,c = canvas.getContext('2d');
			this.canvasMap[canvasKey] = canvas;
			c.font = fontSize+"pt Arial";
			var metrics = c.measureText(s);
			var w = canvas.width = metrics.width;
			var h = canvas.height = size;
			c = canvas.getContext('2d');
			c.font = fontSize+"pt Arial";
			c.textAlign = "center";
			c.textBaseline = "middle";
			c.fillStyle = 'rgba(1.0,1.0,1.0,0)';
			//roundedRect(c,0 , 0 ,w, fontSize );
			c.globalAlpha = 1;
			c.fillStyle = fontColor;
			c.shadowColor = "black";
			c.shadowOffsetX = 0;
			c.shadowOffsetY = 0;
			c.fillText(s, w/2, h/2,w);
			//console.log(c.font);
		}
		return this.canvasMap[canvasKey];
	};
}
OffCanvasManager.get = function(){
	if( !OffCanvasManager.instance )
		OffCanvasManager.instance = new OffCanvasManager();
	return OffCanvasManager.instance;
};

function roundedRect(ctx, x, y, w, h, r) {
	ctx.beginPath();
	ctx.moveTo(x+r, y);
	ctx.lineTo(x+w-r, y);
	ctx.quadraticCurveTo(x+w, y, x+w, y+r);
	ctx.lineTo(x+w, y+h-r);
	ctx.quadraticCurveTo(x+w, y+h, x+w-r, y+h);
	ctx.lineTo(x+r, y+h);
	ctx.quadraticCurveTo(x, y+h, x, y+h-r);
	ctx.lineTo(x, y+r);
	ctx.quadraticCurveTo(x, y, x+r, y);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();
}

function createCirleLabel(text, x, y, z, size, color, backGroundColor) {
	var canvas = document.createElement("canvas");
	var fontSize = parseInt(size*0.5);
	var context = canvas.getContext("2d");
	canvas.width = size;
	canvas.height = size;
	context.font = fontSize + "pt Arial";
	if(backGroundColor) {
		context.fillStyle = backGroundColor;
		context.fillRect(0,0,canvas.width,canvas.height);
	}
	context.textAlign = "center";
	context.textBaseline = "middle";
	context.fillStyle = color;
	context.fillText(text, canvas.width / 2, canvas.height / 2,size);
	var texture = new THREE.Texture(canvas);
	texture.needsUpdate = true;
	var material = new THREE.MeshBasicMaterial({
		map : texture
	});
	var mesh = new THREE.Mesh(new THREE.CircleGeometry(canvas.width, canvas.height), material);
	mesh.doubleSided = true;
	mesh.position.x = x;
	mesh.position.y = y;
	mesh.position.z = z;
	return mesh;
}


