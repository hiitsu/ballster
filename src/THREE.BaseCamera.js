/**
 * Camera subclass that has utility objects as child objects
 *
 * @constructor
 */
THREE.BaseCamera = function(windowHeight){
	THREE.PerspectiveCamera.apply(this,[60, parseInt(windowHeight/2) / windowHeight, 1, 1500]);
	this.textSprite = new THREE.TextSprite(0,0,-200);
	this.textSprite.visible = false;
	this.durationIndicator = new THREE.FilledRectangle(20,-55,-100,2,20,0x0000ff);
	this.durationIndicator.visible = false;
	this.add(this.durationIndicator); // add to camera so its always in front of camera regardless of camera position
	this.add(this.textSprite);
};

THREE.BaseCamera.prototype = Object.create( THREE.PerspectiveCamera.prototype );
THREE.BaseCamera.prototype.constructor = THREE.BaseCamera;

THREE.BaseCamera.prototype.focus = function(obj) {
	var helper = new THREE.BoundingBoxHelper(obj);
	helper.update();
	var boundingSphere = helper.box.getBoundingSphere();
	var center = boundingSphere.center;
	var radius = boundingSphere.radius;
	var distance = center.distanceTo(this.position) - radius;
	var realHeight = Math.abs(helper.box.max.x - helper.box.min.x);
	var depthCorrection = 1.3;
	var fov = 2 * Math.atan(realHeight * depthCorrection / ( 2 * distance )) * ( 180 / Math.PI );
	this.fov = fov;
	this.updateProjectionMatrix();
}