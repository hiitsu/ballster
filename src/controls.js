$(function(){

	$.fn.removeClassPrefix = function(prefix) {
		this.each(function(i, el) {
			var classes = el.className.split(" ").filter(function(c) {
				return c.lastIndexOf(prefix, 0) !== 0;
			});
			el.className = $.trim(classes.join(" "));
		});
		return this;
	};

	var commonViewOptions = {
		model			:	tacmap.stateModel
		,editPoints		:	tacmap.dataWrapper.editPoints
	};

	var SliderView = Backbone.View.extend({
		tagName:'input'
		,className:"slider-input"
		,attributes:{
			type:'range'
			,min:0
			,step:1
			,max:5
		}
		,render:function(){
			this.$el.val(this.model.get('delaySeconds'));
			return this;
		}
	});
	var SliderAndValueView = Backbone.View.extend({
		className:'slider-wrapper'
		,initialize:function(options){
			this.sliderView = new SliderView(options);
			this.model.on('change:delaySeconds',function(m,v){
				this.$('span').text(v);
			},this);
		}
		,render:function(options){
			this.$el.html('<span class="slider-value">'+this.model.get('delaySeconds')+'</span>');
			this.$el.prepend(this.sliderView.render().el);
			return this;
		}
	});
	var TogglableSliderView = Backbone.View.extend({
		tagName:'span'
		,className:'tile tile100'
		,events:{
			'click':'showSlider',
			'change input':'changeValue',
			'input input':'changeValue'
		}
		,initialize: function(options) {
			Backbone.View.prototype.initialize.apply(this, arguments);
			this.options = options;
			this.hideSlider = _.debounce(_.bind(this._hideSlider, this), 1000);
		}
		,render:function(){
			this.$el.text(this.options.text+':'+this.model.get('delaySeconds')+'s');
			return this;
		}
		,showSlider:function(e){
			if( !this.sliderView )
				this.sliderView = new SliderAndValueView({model:this.model});
			this.$el.html(this.sliderView.render().el);
			if( this.timeoutId )
				clearTimeout(this.timeoutId);
		}
		,changeValue:function(e){
			this.model.set('delaySeconds',parseInt(this.$('input').val()));
			this.hideSlider();
			return true;
		}
		,_hideSlider:function(e){
			this.render();
		}

	});

	var MenuView = Backbone.View.extend({
		initialize:function() {
			Backbone.View.prototype.initialize.apply(this, arguments);
			this.model.on('change:state',function(m,v){
				this.$el.toggle(!/(animating|exporting)/i.test(v));
			},this);
		}
		,render:function(){
			this.$el.html(
				'<div class="menu-header menu-'+this.color+'-header">'+this.title+'</div>'+
				'<div class="menu-'+this.color+'-content menu-content"></div>'
			);
			if( this.view || this.html )
				this.$('.menu-content').append(this.view ? this.view.render().el : this.html);
			else if( this.views ) {
				_.each(this.views,function(v){
					this.$('.menu-content').append( !_.isString(v) ? v.render().el : v );
				},this);
			}
			return this;
		}
		/*,changeView:function(view) {
			if( this.view )
				this.view.remove();
			this.view = view;
			this.$('.menu-content').append(this.view.render().el);
		}*/
		,remove:function(){
			if( this.view )
				this.view.remove();
			Backbone.View.prototype.remove.call(this);
		}
	});

	var PresetMenuView = MenuView.extend({
		el:'.menu.menu-presets'
		,initialize:function(){
			MenuView.prototype.initialize.apply(this, arguments);
			this.color = 1;
			this.title = 'preset';
			this.html =
				'<div class="row100 preset"><div class="tile tile100">4 vs 4</div></div>'+
				'<div class="row100 preset"><div class="tile tile100">6 vs 6</div></div>'+
				'<div class="row100 preset"><div class="tile tile100">10 vs 10</div></div>';
		}
		,events:{
			"click .preset":"resetTo"
			,"click li.gif":"gif"
			,"click li.save":"save"
		}
		,resetTo:function(e){
			tacmap.resetTo(parseInt($(e.currentTarget).text().split('v')[0]));
		}
		,gif:function(e){
		}
		,save:function(e){
			tacmap.save();
		}
	});
	var AddView = Backbone.View.extend({
		className:'add'
		,events:{
			'click .wrapper':'add'
		}
		,initialize:function(options){
			this.editPoints = options.editPoints;
		}
		,render:function(){
			this.$el.html('<div class="wrapper">add +</div>');
			return this;
		}
		,add:function(e){
			this.editPoints.add(new EditPointModel());
		}
	});

	var EditPointEditorView = Backbone.View.extend({
		tagName:'div'
		,className:'row100'
		,events:{
			'click .camera' : 'changeCamera'
		}
		,initialize:function(options){
			this.delaySliderView = new TogglableSliderView({model:this.model,text:'delay'});
			this.index = options.index;
		}
		,render:function(){
			this.$el.html(
				'<span class="tile tile25 selector">'+(this.index+1)+'</span>' +
				'<span class="tile tile25 camera helicopterView " data-cameraType="helicopterView" > </span>' +
				'<span class="tile tile25 camera stadiumView" data-cameraType="stadiumView" > </span>' +
				'<span class="tile tile25 camera goaltenderView" data-cameraType="goaltenderView"> </span>'
			);
			this.$("[data-cameraType='"+this.model.get('cameraType')+"']").addClass('selected');
			this.$el.append(this.delaySliderView.render().el);
			return this;
		}
		,changeCamera:function(e){
			e.preventDefault();
			this.$('.camera').removeClass('selected');
			var $camera = $(e.currentTarget).addClass('selected');
			this.model.set('cameraType',$camera.attr('data-cameraType'));
			tacmap.update();
			return false;
		}
	});

	var EditPointListView = Backbone.View.extend({
		className:"editpoint-listview"
		,initialize:function(options){
			this.editPoints = options.editPoints;
			options.editPoints.on('add remove reset',function(){
				this.render();
			},this);
		}
		,events:{
			'click .selector':'select'
		}
		,render: function() {
			this.$el.empty();
			this.editPoints.each(function(m,i){
				this.$el.append(new EditPointEditorView({model:m,index:i}).render().el);
			},this);
			this.$('.selector').eq(this.model.get('editPointIndex')).addClass('selected');
			return this;
		}
		,select:function(e){
			var $e = $(e.currentTarget);
			this.$('.selector').removeClass('selected');
			$e.addClass('selected');
			var n = parseInt($e.text())-1;
			this.model.set('editPointIndex',n);
			return false;
		}
	});

	var EditPointMenuView = MenuView.extend({
		el: '.menu.menu-editpoints'
		,initialize:function(options){
			MenuView.prototype.initialize.apply(this, arguments);
			this.color = 2;
			this.title = 'moves';
			this.views = [new EditPointListView(options), new AddView(options)];
		}
	});

	var PlayerEditView = Backbone.View.extend({
		events:{
			'change input':'changeNumber'
			,'blur input':'changeNumber'
			,'click input':'preselect'
			,'click .lock-ball':'lockBall'
			,'click .unlock-ball':'unlockBall'
			,'click .removePlayer':'removePlayer'
		}
		,initialize:function(options){
			this.item = options.item;
			this.model.on('change:hammering',this.updateUI,this);
		}
		,preselect:function(){
			this.$('input').select();
		}
		,changeNumber:function(){
			this.item.numberBadge = this.$('input').val();
			tacmap.scene.replace(this.item.object3D());
			tacmap.update();
		}
		,render: function () {
			this.$el.html('' +
				'<span class="tile tile100">number: <input maxlength="2" type="text" size="2" value="'+this.item.numberBadge+'"/></span>'+
				'<span class="tile tile100 lock-ball">lock ball</span>'+
				'<span class="tile tile100 unlock-ball">unlock ball</span>'+
				'<span class="tile tile100 removePlayer">delete</span>'
			);
			this.updateUI();
			return this;
		}
		,updateUI:function(){
			var isBallLocked = !!tacmap.isBallLocked();
			var canSelectedReachTheBall = tacmap.canSelectedReachTheBall();
			var hasSelectedPlayerTheBall = tacmap.hasSelectedPlayerTheBall();
			var showLock = !!(!isBallLocked && canSelectedReachTheBall);
			this.$('.lock-ball').toggle(showLock);
			var showUnlock = !!(isBallLocked && hasSelectedPlayerTheBall);
			this.$('.unlock-ball').toggle(showUnlock);
		}
		,lockBall:function(){
			tacmap.lockBall();
			tacmap.update();
			this.updateUI();
		}
		,unlockBall:function(){
			tacmap.unlockBall();
			tacmap.update();
			this.updateUI();
		}
		,removePlayer:function(){
			tacmap.deleteSelected();
			tacmap.update();
			this.updateUI();
		}
		,remove:function(){
			this.changeNumber();
			Backbone.View.prototype.remove.call(this);
		}
	});

	var PlayerContextMenuView = namespace.PlayerContextMenuView = namespace.GoalkeeperContextMenuView = MenuView.extend({
		className:'menu menu-bar menu-player',
		initialize:function(options){
			MenuView.prototype.initialize.apply(this, arguments);
			this.color = 2;
			this.title = 'Player';
			this.view = new PlayerEditView(options);
		}
	});

	var BallContextMenuView = namespace.BallContextMenuView = MenuView.extend({
		className:'menu menu-bar menu-ball',
		initialize:function(options){
			MenuView.prototype.initialize.apply(this, arguments);
			this.color = 2;
			this.title = 'Ball';
			this.html = 'be the ball';
		}
	});

	var contextMenu = null;
	tacmap.stateModel.on('change',function(m){
		if( /single-selected/.test(m.get('state')) ) {
			if( contextMenu )
				contextMenu.remove();
			var viewConstructor = m.selectedClass()+"ContextMenuView";
			if( !namespace[viewConstructor] ) {
				console.warn('no ctor for '+viewConstructor);
				return;
			}
			contextMenu = new namespace[viewConstructor](_.extend({},commonViewOptions,{item: m.get('selected')[0]}));
			$('body').append(contextMenu.render().el);
			contextMenu.$el.show();
		} else {
			if( contextMenu )
				contextMenu.remove();
		}
	});

	var presetMenuTogglerView = new PresetMenuView(commonViewOptions).render();
	var editPointMenuView = new EditPointMenuView(commonViewOptions).render();

	$(window).on('resize orientationchange',tacmap.resize);

});


/*		,events:{
 'click.toggler':'click'
 }
 ,initialize:function(){
 this.model.on('change:state',function(m,v){
 this.$el.toggle(!/(animating|exporting)/i.test(v));
 },this);
 this.model.on('change:hammering',function(m,v){
 if( this.subview )
 this.subview.$el.hide();
 },this);
 }
 ,render:function(){
 this.$el.html('<div class="toggler menu-1-header">presets</div>');
 }
 ,click:function(){
 if( !this.subview ) {
 this.subview = new PresetMenuView();
 this.$el.append(this.subview.render().el);
 } else
 this.subview.$el.toggle();
 }
 */
/*
 var EditPointPropertyView = Backbone.View.extend({
 el:'.menu.editpoint-context-menu'
 ,events:{
 'click .remove':'removeEditpoint',
 'click .passing-lanes':'togglePassingLanes',
 'click .number-badges':'toggleNumberBadges',
 'change input.delay':'changeDelay'
 ,'focus input.delay':'stopHideTimer'
 }
 ,initialize:function(options){
 this.model.on('change:state',function(m,v){
 var animating = /(animating|exporting)/i.test(v);
 if( animating )
 this.$el.hide();
 },this);
 this.model.on('change:editPointIndex',this.updateUI,this);
 this.editPoints = options.editPoints;
 this.editPoints.on('add remove reset',this.updateUI,this);
 this.model.on('change:hammering',function(m,v){
 this.$el.hide();
 },this);
 }
 ,removeEditpoint:function(e){
 e.preventDefault();
 tacmap.deleteEditPoint();
 return false;
 }
 ,togglePassingLanes:function(e){
 e.preventDefault();
 var o = tacmap.dataWrapper.editPoints.at(this.model.get('editPointIndex'));
 o.set('showPassingLanes', !o.get('showPassingLanes'));
 this.model.set('userInteraction',(this.model.get('userInteraction') || 0)+1);
 this.updateUI();
 return false;
 }
 ,toggleNumberBadges:function(e){
 e.preventDefault();
 var o = tacmap.dataWrapper.editPoints.at(this.model.get('editPointIndex'));
 o.set('showNumberBadges', !o.get('showNumberBadges'));
 this.model.set('userInteraction',(this.model.get('userInteraction') || 0)+1);
 this.updateUI();
 return false;
 }
 ,changeDelay:function(e){
 e.preventDefault();
 var o = this.editPoints.at(this.model.get('editPointIndex'));
 var v = parseInt($(e.currentTarget).val());
 o.set('delaySeconds',v);
 this.updateUI();
 return false;
 }
 ,show:function(){
 this.updateUI();
 this.$el.show();
 if(this.timeoutId) clearTimeout(this.timeoutId);
 var self = this;
 this.timeoutId = setTimeout(function(){
 self.$el.hide();
 },2000);
 }
 ,stopHideTimer:function(){
 if( this.timeoutId ) clearTimeout(this.timeoutId);
 }
 ,updateUI:function(){
 this.$('.remove').toggle(this.editPoints.length > 1);
 var delay = this.editPoints.at(this.model.get('editPointIndex')).get('delaySeconds');
 this.$('.value').text(delay);
 this.$('.delay').val(delay);
 this.$('.passing-lanes').toggleClass('on',!!this.editPoints.at(this.model.get('editPointIndex')).get('showPassingLanes'));
 this.$('.number-badges').toggleClass('on',!!this.editPoints.at(this.model.get('editPointIndex')).get('showNumberBadges'));
 }
 ,render:function(){
 this.$el.html('<ul><li class="remove">X</li><li class="passing-lanes">X</li><li class="number-badges">X</li></ul>');
 this.$el.append('<form><label for="delay">delay:</label><input name="delay" class="delay" type="range" min="0" max="5" step="1" value="0" /><div class="value"></div></form>');
 this.$el.hide();
 return this;
 }
 });
 var editPointPropertyView = new EditPointPropertyView(commonViewOptions).render();
var EditPointSelectorView = Backbone.View.extend({
	className:'tile25'
	,render:function(){
		this.$el.text('1');
	}
});
*/
