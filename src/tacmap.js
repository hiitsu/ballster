var EditPointModel = Backbone.Model.extend({
	defaults: {
		text:''				 			// scene wide text to clarify what is happening
		,delaySeconds:0			 		// delay to let user read the text
		,showPassingLanes:false			// whether to show passing lanes for other players
		,showNumberBadges:true			// whether to show players' numbers or initial badge next to the 3d object
		,areas:[]			 			// possibly highlighted areas
		,cameraType:'helicopterView'	// which camera angle to use for this editPoint animation
		,playerWithBall:null 			// id of the player that can dribble with ball
		,movementPointMap:{} 			// itemID => array of {x:0,y:0},first pos of each item should be here
	}
});

var EditPointCollection = Backbone.Collection.extend({
	model: EditPointModel
});

function DataWrapper() {
	this.itemManager = new ItemManager();
	this.cameraType = function(editPointIndex,cameraType){
		if( cameraType ) this.editPoints.at(editPointIndex).set('cameraType',cameraType);
		return this.editPoints.at(editPointIndex).get('cameraType');
	};
	this.toJSON = function() {
		return {
			items: _.map(this.itemManager.items,function(o) {
				o.klass = o.constructor.name;
				return o;
			})
			,editPoints:this.editPoints
		};
	};
	this.setData = function(data){
		this.itemManager.items = _.map(data.items,function(item){
			return new window[item.klass](item);
		});
		this.editPoints = data.editPoints;
	};
	this.reset = function() {
		this.itemManager.clear();
		if( !this.editPoints )
			this.editPoints = new EditPointCollection();
		this.editPoints.reset([_.cloneDeep(EditPointModel.prototype.defaults)]);
	};
	this.reset();
	this.lockBall = function(editPointIndex){
		var playerId = this.itemManager.playerWithBall().id;
		this.editPoints.at(editPointIndex).set('playerWithBall',playerId);
		var map = this.editPoints.at(editPointIndex).get('movementPointMap');
		map[this.itemManager.ball().id] = _.cloneDeep(map[playerId][0]);
	};
	this.unlockBall = function(editPointIndex){
		this.editPoints.at(editPointIndex).set('playerWithBall',null);
	};
	this.isBallLocked = function(editPointIndex){
		var id = this.editPoints.at(editPointIndex).get('playerWithBall');
		return id ? this.itemManager.itemById(id) : null;
	};
	this.isBallLockable = function(id){
		return this.itemManager.isPlayer(id) && this.itemManager.canPossesBall(id);
	};
	this.addArea = function(){
		this.editPoints.at(editPointIndex).areas.push([]);
	};
	this.removeEditPoint = function(editPointIndex){
		this.editPoints.remove(this.editPoints.at(editPointIndex));
	};
	this.addEditPoint = function(editPointIndex){
		var editPoint = this.editPoints.last() //(editPointIndex);
			,newEditPoint = new EditPointModel(_.cloneDeep(editPoint.attributes))
			,withBall = editPoint.get('playerWithBall');
		_.each(newEditPoint.get('movementPointMap'),function(points,key,map){
			map[key] = points && points.length ? points.splice(-1) : [];
			if( withBall == key ) {
				var ball = this.itemManager.itemByType(Ball);
				if( ball )
					map[ball.id] = points.splice(-1);
			}
		},this);
		this.editPoints.add(newEditPoint/*,{at:editPointIndex}*/);
	};
	this.changeToEditPoint = function(n){
		_.each(this.editPoints.at(n).get('movementPointMap'),function(values,id){
			this.itemManager.moveTo(id,values[0].x, values[0].y);
		},this);
	};
	this.addItem = function(item){
		this.itemManager.add(item);
		if( !item.movable() )
			return;
		// if there are multiple edit points already, each of them will have same start position
		this.editPoints.each(function(editPoint){
			editPoint.get('movementPointMap')[this.id] = [{ x: this.x, y: this.y }];
		},item);
	};
	this.deleteItem = function(idOrItem){
		var id = idOrItem instanceof Item ? idOrItem.id : idOrItem;
		this.itemManager.deleteItemById(id);
		this.editPoints.each(function(editPoint){
			delete editPoint.get('movementPointMap')[id];
		});
	};
	this.resetMovementById = function(editPointIndex,id){
		var p = this.itemManager.itemById(id);
		this.editPoints.at(editPointIndex).get('movementPointMap')[id] = [{x: p.x, y: p.y }];
	};
	this.moveTo = function(editPointIndex,id,x,y){
		var map = this.editPoints.at(editPointIndex).get('movementPointMap');
		map[id] = [{x:x,y:y}];
		this.itemManager.moveTo(id,x,y);
		if( this.editPoints.at(editPointIndex).get('playerWithBall') == id ) {
			var ballId = this.itemManager.ball().id;
			this.itemManager.moveTo(ballId, x, y);
			map[ballId] = [{x:x,y:y}];
		}

	};
	this.resetTo = function(howManyFieldPlayers){
		this.reset();
		var field = new StripedField();
		this.addItem(field);
		var goalA = new ModelledFootballGoal({team:Item.TEAM_A});
		field.positionForGoal(goalA);
		this.addItem(goalA);
		var goalB = new ModelledFootballGoal({team:Item.TEAM_B});
		field.positionForGoal(goalB);
		this.addItem(goalB);
		this.addItem(new Goalkeeper(_.extend(goalA.positionForGoalkeeper(),{numberBadge:'1',team:Item.TEAM_A })));
		this.addItem(new Goalkeeper(_.extend(goalB.positionForGoalkeeper(),{numberBadge:'1',team:Item.TEAM_B })));
		this.addItem(new FancyBall(field.positionForBall()));
		//this.itemManager.add(new Player(field.positionForPlayer(Item.TEAM_A,0),Item.TEAM_A));
		var playersPerRow = howManyFieldPlayers/2;
		for(var i=0; i < howManyFieldPlayers; i++) {
			this.addItem(new Player(_.extend(field.positionForPlayer(Item.TEAM_A,i,playersPerRow),{ numberBadge:''+(i+2),team: Item.TEAM_A})));
			this.addItem(new Player(_.extend(field.positionForPlayer(Item.TEAM_B,i,playersPerRow),{ numberBadge:''+(i+2),team: Item.TEAM_B})));
		}
	};
	/**
	 * @return Total amount of different frames in all editpoints
	 */
	this.totalFrames = function(){
		var ticks = this.editPoints.reduce(function(memo,editPoint){
			var longestAnimationLine = _.max(_.map(editPoint.get('movementPointMap'),function(v) { return v ? (v.length || 0) : 0; }));
			return memo + longestAnimationLine;
		},0,this);
		return ticks;
	};
	/**
	 * @param n				Go to n:th frame in current editpoint.
	 * @returns {boolean}	Return true if n bigger than total number of animatable frames in current editpoint, useful for looping when when animating
	 */
	this.toFrame = function(editPointIndex,n){
		//var angleLookupMax = 3;
		var finishedCount = 0, movementPointMap = this.editPoints.at(editPointIndex).get('movementPointMap');
		_.each(movementPointMap,function(points,id){
			var item = this.itemManager.itemById(id);
			if( item == null )
				return;
			if( !points || !points.length || n >= points.length-1 ) {
				finishedCount++;
				return;
			}
			item.x = points[n].x;
			item.y = points[n].y;
			//if( m > angleLookupMax ) {
			//	_xy = a[m-angleLookupMax];
			//	item.angle = (float) Math.atan2(_xy[1]-xy[1],_xy[0]-xy[0]);
			//}
		},this);
		var editPointFinished = finishedCount == _.size(movementPointMap);
		return editPointFinished;
	};
	this.addPointToArray = function(a,x,y){
		if( a.length > 0 ){
			var lastX = a[a.length-1].x;
			var lastY = a[a.length-1].y;
			var MINIMUM_MOVEMENT = 3;
			if( Math.sqrt(Math.pow(x-lastX,2)+Math.pow(y-lastY,2)) < MINIMUM_MOVEMENT )
				return;
		}
		a.push({x:x,y:y});
	};
	this.addMovementPoint = function(editPointIndex,id,x,y){
		var o = this.editPoints.at(editPointIndex).get('movementPointMap');
		if( !o[id] ) {
			o[id] = [];
		}
		this.addPointToArray(o[id],x,y);
	};
	this.getArea = function(editPointIndex,areaIndex){
		if( areaIndex )
			return this.editPoints.at(editPointIndex).areas[areaIndex];
		return _.last(this.editPoints.at(editPointIndex).areas);
	};
	this.addAreaPoint = function(x,y){
		var o = this.editPoints.at(editPointIndex).areas;
		this.addPointToArray(o[o.length-1],x,y);
	};
	this.update = function(editPointIndex,scene,isAnimating){
		var currentEditPoint = this.editPoints.at(editPointIndex)
			,movementPointMap = currentEditPoint.get('movementPointMap');

		this.itemManager.update(scene);

		scene.toggle("numberbadge",!!currentEditPoint.get('showNumberBadges'));

		_.each(_.range(0,20),function(i){
			var n = 'passinglane-'+i;
			var o = this.getObjectByName(n);
			if( o || !currentEditPoint.get('showPassingLanes') )
				this.remove(o);
		},scene);
		var playerLockedToBall = this.isBallLocked(editPointIndex);
		if( playerLockedToBall ) {
			var ball = this.itemManager.itemByType(Ball);
			this.itemManager.moveTo(ball.id,playerLockedToBall.x,playerLockedToBall.y);
		}
		var withBall = playerLockedToBall || this.itemManager.playerWithBall();
		var o = scene.getObjectByName('shootingsector');
		if( o ) scene.remove(o);
		if( withBall ){
			if( currentEditPoint.get('showPassingLanes') ) {
				var lanes = this.itemManager.passingLanesForPlayer(withBall.id);
				_.each(lanes, function (a, i) {
					a[0].z = a[1].z = 25;
					var dir = a[1].clone().sub(a[0]).normalize();
					var d = a[0].distanceTo(a[1]) * 0.8;
					var o = new THREE.ArrowHelper(dir, a[0], d, 0x0000ff, 16, 12);
					o.line.material.linewidth = 3;
					o.name = 'passinglane-' + i;
					this.add(o);
				}, scene);
			}
			var sector = this.itemManager.shootingSectorForPlayer(withBall.id);
			if( sector ) {
				var material = new THREE.MeshBasicMaterial({
					color: 0x222222, opacity: 0.5, transparent:true
				});
				var segments = 64;
				var circleGeometry = new THREE.CircleGeometry( sector.radius, segments, sector.angle, sector.theta );
				var mesh = new THREE.Mesh(circleGeometry, material);
				mesh.name = 'shootingsector';
				mesh.position.set(sector.x,sector.y,3);
				scene.add(mesh);
			}
		}
		if( isAnimating )
			scene.remove(scene.getObjectByName('focusRing',true));

		_.each(movementPointMap,function(points,id){
			var movementLineId = 'movement'+id;
			if( isAnimating || !points || points.length <= 1 )
				return scene.remove(scene.getObjectByName(movementLineId,true));
			var item = this.itemManager.itemById(id);
			if( !item )
				return;
			return scene.lineWith(movementLineId,points,item.color());
		},this);
	};
}


var fsm = {
	"state-no-selection": {
		touch:function(){
			var editPointIndex = this.stateModel.get('editPointIndex');
			var item = this.dataWrapper.itemManager.selectMovableItemCloseByXY(this.input[0].x, this.input[0].y,this.dataWrapper.isBallLocked(editPointIndex));
			if( item ) {
				this.stateModel.set({state:"state-single-selected",selected:[item]});
				this.scene.focusRing(item.x,item.y);
			}
			return true;
		}
		,hold:function(){
			this.dataWrapper.addArea();
			this.stateModel.set('state',"state-draw-area-points");
		}
		,doubletap  : function(){
			this.stadiumView();
			this.stateModel.set('state',"state-animating");
		}
	}
	,"state-draw-area-points": {
		release:function(){
			var tempLine = this.scene.getObjectByName('tempLine'),
				posX = tempLine.geometry.vertices[0].x,
				posY = tempLine.geometry.vertices[0].y,
				height = -(posY-tempLine.geometry.vertices[tempLine.geometry.vertices.length-1].y),
				width = -(posX-tempLine.geometry.vertices[tempLine.geometry.vertices.length-1].x),
				rectArea = areaRectangle(posX,posY,2,width,height,0x0000ff,true,0.35);
				rectArea.name = "areaRectangle";
			this.scene.remove(tempLine);
			this.update();
			this.scene.add(rectArea);
			this.stateModel.set('state',"state-no-selection");
		}
		,drag:function(){
			this.dataWrapper.addAreaPoint(this.input[0].x,this.input[0].y);
			this.scene.lineWith('tempLine',this.dataWrapper.getArea());
		}
	}
	,"state-single-selected": {
		cleanup:function(){
			this.scene.remove(this.scene.getObjectByName('focusRing',true));
		}
		,touch:function(){
			var editPointIndex = this.stateModel.get('editPointIndex');
			var item = this.dataWrapper.itemManager.selectMovableItemCloseByXY(this.input[0].x, this.input[0].y,this.dataWrapper.isBallLocked(editPointIndex));
			if( item ) {
				this.stateModel.set('selected',[item]);
				this.scene.focusRing(item.x,item.y);
			} else {
				this.stateModel.set('state',"state-no-selection");
			}
			return true;
		}
		,editPointChanged:function(){
			var item = this.dataWrapper.itemManager.itemById(this.stateModel.get('selected')[0].id);
			this.scene.focusRing(item.x,item.y);
		}
		,hold:function(){
			var editPointIndex = this.stateModel.get('editPointIndex');
			this.dataWrapper.resetMovementById(editPointIndex,this.stateModel.get('selected')[0].id);
			this.stateModel.set('state',"state-draw-movement-points");
		}
		,drag:function(){
			var editPointIndex = this.stateModel.get('editPointIndex');
			var id = this.stateModel.get('selected')[0].id;
			this.dataWrapper.moveTo(editPointIndex,id,this.input[0].x,this.input[0].y);
			this.scene.focusRing(this.input[0].x,this.input[0].y);
			this.dataWrapper.itemManager.rotateTo(id, this.trendAngle());
		}
	}
	,"state-draw-movement-points": {
		release:function(){
			this.stateModel.set('state',"state-single-selected");
		}
		,drag:function(){
			var editPointIndex = this.stateModel.get('editPointIndex');
			var id = this.stateModel.get('selected')[0].id;
			this.dataWrapper.addMovementPoint(editPointIndex,id,this.input[0].x,this.input[0].y);
		}
	}
	,"state-animating":{
		init:function(){
			this.currentAnimationEditPointIndex = 0;
			this.frameNumber = 0;
			this.accumulatedFrameCount = 0;
			this.totalFrames = this.dataWrapper.totalFrames();
			this.camera.durationIndicator.visible = true;
			this.camera.durationIndicator.setFill(0);
		}
		,doubletap  : function(){
			this.camera.durationIndicator.visible = false;
			this.accumulatedFrameCount = 0;
			this.stateModel.set('state',"state-no-selection");
		}
		,tick:function(){
			var fill = this.totalFrames > 1 ? parseFloat(this.accumulatedFrameCount)/parseFloat(this.totalFrames-1) : 1.0;
			this.camera.durationIndicator.setFill(fill);
			this.update(true,this.currentAnimationEditPointIndex);
			this.accumulatedFrameCount++;
			var editPointFinished = this.dataWrapper.toFrame(this.currentAnimationEditPointIndex,this.frameNumber);
			if( this.frameNumber == 0 ){
				var seconds = this.dataWrapper.editPoints.at(this.currentAnimationEditPointIndex).get('delaySeconds');
				if( seconds ) {

				}
			}
			this.frameNumber++;
			if(editPointFinished) {
				this.frameNumber = 0;
				this.currentAnimationEditPointIndex++;
				if( this.currentAnimationEditPointIndex > this.dataWrapper.editPoints.length-1 ) {
					this.accumulatedFrameCount = 0;
					this.currentAnimationEditPointIndex = 0;
					this.nextTickEpoch = Date.now()+1500;
					this.camera.textSprite.setText('the end','pink',50,1000);
					this.update(true,this.currentAnimationEditPointIndex-1);
				}
			}
		}
		,cleanup:function(){
			this.camera.textSprite.visible = false;
			this.stateModel.trigger('change:editPointIndex',this.stateModel, this.stateModel.get('editPointIndex'));
		}
	}
	/*,"state-exporting":{
		init:function(){
			this.gif = new GIF({
				workers: 4,
				quality: 10
			});
			this.frameNumber = 0;
			var self = this;
			this.gif.on('finished', function(blob) {
				var blobURL = (window.URL || window.webkitURL).createObjectURL(blob);
				window.open(blobURL,'_blank');
				//$('body').append('<a class="blob" />');
				//$('a.blob')[0].href = blobURL;
				delete self.gif;
			});
		}
		,doubletap  : function(){s
			this.stateModel.set('state',"state-no-selection");
			this.helicopterView();
			this.update();
		}
		,tick:function(){
			this.update(true);
			var editPointFinished = this.dataWrapper.toFrame(this.frameNumber++), loopFlag = false;
			var delayMillis = 100;
			if(editPointFinished) {
				this.frameNumber = 0;
				delayMillis = parseInt(this.dataWrapper.getEditPointDelaySeconds())*1000;
				this.nextTickEpoch = Date.now()+delayMillis;
				loopFlag = this.dataWrapper.nextEditPoint();
				console.log('loop');
			}
			this.gif.addFrame(this.renderer.domElement, {copy:true,delay: delayMillis});
			if(loopFlag) {
				this.gif.render();
				this.update();
				this.stateModel.set('state',"state-no-selection");
				this.helicopterView();
			}
		}
	}*/
};

/**
 * State of the application, non-persistent, model to let views listen to state changes, for example hide when animating, and show certain contextmenus
 * when single item of different types or multiple selected
 */
var StateModel = Backbone.Model.extend({
	defaults: {
		state:'state-no-selection',	// currently FSM state name
		selected:null,				// array of selected items
		editPointIndex:0,			// current editpoint index
		hammering:0					// helper to know if any hammer.js activity on 3D canvas
	}
	,isSelectedPlayer:function(){
		var flag = (this.get('selected') && (this.get('selected')[0] instanceof Player));
		return flag;
	}
	,selectedClass:function(){
		var arr = this.get('selected');
		return arr && arr[0] ? arr[0].constructor.name : null;
	}
});

/**
 *
 * @param domElement	JQuery compatible selector string or actual dom element
 * @param sketchId		Server id for the sketch for saving
 * @constructor
 */
function TacMap(domElement,sketchId){
	this.sketchId = sketchId;
	this.$container = $(domElement);
	this.projector = new THREE.Projector();
	this.raycaster = new THREE.Raycaster();
	var windowHeight = $(window).height();
	this.camera = new THREE.BaseCamera(windowHeight);
	this.scene = new THREE.BaseScene();
	this.scene.add(this.camera);
	_.each(fsm,function(state,stateName){
		this[stateName] = state;
	},this);

	this.renderer = new THREE.WebGLRenderer({antialias:false});
	this.renderer.setClearColor(0xefefef);
	this.$container.append(this.renderer.domElement);
	this.dataWrapper = new DataWrapper();

	this.stateModel = new StateModel();
	this.stateModel.on('change:state',function(m,v) {
		var oldState = m.previous('state');
		if( this[oldState].cleanup ) this[oldState].cleanup.apply(this);
		if( this[v].init ) this[v].init.apply(this);
		console.log(oldState+' -> '+v);
	},this);
	this.stateModel.on('change:editPointIndex',function(m,v) {
		_.each(this.dataWrapper.editPoints.at(v).get('movementPointMap'),function(points,id){
			if( !points || !points.length ) return;
			this.dataWrapper.itemManager.moveTo(id,points[0].x, points[0].y);
		},this);
		if( this[this.stateModel.get('state')].editPointChanged )
			this[this.stateModel.get('state')].editPointChanged.call(this);
		this.update();
	},this);
	this.stateModel.on('change:userInteraction',function(){ this.update();},this);


	this.helicopterView = function(){
		this.changeCamera('helicopterView',0,0,950,0,1,0);
	};
	this.stadiumView = function(){
		this.changeCamera('stadiumView',950,0,450,-1,0,0);
	};
	this.goaltenderView = function(){
		this.changeCamera('goaltenderView',0,900,250,0,-1,0);
	};
	this.changeCamera = function(cameraType,x,y,z,upX,upY,upZ) {
		this.camera.position.set(x,y,z);
		this.camera.up = new THREE.Vector3(upX,upY,upZ);
		this.camera.lookAt(new THREE.Vector3(0, 0, 0));
		this.camera.updateProjectionMatrix();
		this.scene.triggerEvent({type:'cameraChange',cameraType:cameraType});
	};
	this.goaltenderView();

	this.input = [];
	this.addInput = function(x,y){
		this.input.unshift({x:x,y:y});
		if( this.input.length > 3 )
			this.input.pop();
	};
	this.trendAngle = function(){
		if( !this.input.length )
			return 0;
		return Math.atan2(this.input[0].y-this.input[this.input.length-1].y,this.input[0].x-this.input[this.input.length-1].x);
	};
	this.load = function(animationData){
		this.scene.reset();
		this.dataWrapper.setData(animationData);
		this.update();
	};
	this.save = function(tags){
		var gif = new GIF({workers: 2,quality: 10})
			,self = this;

		gif.on('finished', function(blob) {
			var formData = new FormData();
			formData.append("file",blob);

			//TODO use promises
			//TODO update preview image
			$.ajax({
				url: "/api/file",
				type: "POST",
				data: formData,
				processData: false,
				contentType: false,
				success:function(image,status){
					console.log(image);
					$.ajax({
						url		:	"/api/sketch" + (self.sketchId ? ('/'+self.sketchId) : ''),
						type	:	self.sketchId ? "PUT": "POST",
						data:JSON.stringify({
							previewImage		:	image._id
							,tags				:	tags || []
							,animationData		:	self.dataWrapper
						}),
						contentType:"application/json; charset=utf-8",
						dataType:"json",
						success: function(sketch){
							self.sketchId = sketch._id;
							history.replaceState( {} , sketch.slug, '/edit/'+sketch._id );
							console.log(sketch);
						}
					});
				}
			});
		});
		this.update();
		gif.addFrame(this.renderer.domElement, {copy:true,delay:100});
		gif.render();
		/*var d = this.renderer.domElement.getContext().getImageData();
		console.log(d);
		console.log(this.renderer.domElement.toDataURL());*/

	};
	this.getEditPointCount = function(){
		return this.dataWrapper.editPoints.length;
	};
	this.getCurrentEditPoint = function(){
		return this.stateModel.get('editPointIndex');
	};
	this.isBallLocked = function(){
		return this.dataWrapper.isBallLocked(this.stateModel.get('editPointIndex'));
	};
	this.canSelectedReachTheBall = function(){
		var selected = this.stateModel.get('selected');
		if( !selected || !selected[0] )
			return false;
		return this.dataWrapper.itemManager.canReachTheBall(selected[0]);
	};
	this.hasSelectedPlayerTheBall = function(){
		var selected = this.stateModel.get('selected');
		if( !selected || !selected[0] )
			return false;
		return this.dataWrapper.itemManager.hasBall(selected[0]);
	};
	this.lockBall = function(){
		this.dataWrapper.lockBall(this.stateModel.get('editPointIndex'),this.stateModel.get('selected')[0].id)
	};
	this.unlockBall = function(){
		this.dataWrapper.unlockBall(this.stateModel.get('editPointIndex'));
	};
	this.addEditPoint = function(){
		this.dataWrapper.addEditPoint();
	};
	this.deleteEditPoint = function(){
		var n = this.stateModel.get('editPointIndex');
		this.dataWrapper.editPoints.remove(this.dataWrapper.editPoints.at(n));
		var len = this.dataWrapper.editPoints.length;
		if( n > len-1 )
			this.stateModel.set('editPointIndex',--n);
	};
	this.resetTo = function(howMany){
		this.stateModel.set(_.cloneDeep(StateModel.prototype.defaults));
		this.scene.reset();
		this.renderer.render(this.scene,this.camera);
		this.dataWrapper.resetTo(howMany);
		this.update();
		var self = this;
		setTimeout(function(){ self.update(); },0);
	};
	this.deleteSelected = function(){
		var id = this.stateModel.get('selected')[0].id;
		this.dataWrapper.deleteItem(id);
		this.scene.remove(this.scene.getObjectByName(id));
		this.scene.remove(this.scene.getObjectByName("movement"+id));
		this.scene.remove(this.scene.getObjectByName("focusRing"));
		var self = this;
		setTimeout(function(){
			self.stateModel.set({state:"state-no-selection",selected:null});
			//self.stateModel.trigger('change:state',self.stateModel,"state-no-selection");
		},0);

	};
	this.dispose = function(){
		this.scene.dispose();
		this.$container.html('');
		this.renderer = null;
		return true;
	};
	this.update = function(isAnimating,n){
		var editPointIndex = _.isNumber(n) ? n : this.stateModel.get('editPointIndex');
		this[this.dataWrapper.cameraType(editPointIndex)]();
		this.dataWrapper.update(editPointIndex,this.scene,isAnimating);
		this.renderer.render(this.scene,this.camera);
	};
	/**
	 * This handles all event regarding the 3D WebGL canvas
	 * @param e
	 */
	this.handleHammer = function(e) {
		console.log(e ? e.type :'notype');
		if( /(transform)/i.test(e.type) ) {
			this.camera.position.z += ((-1+e.gesture.scale)*50);
			console.log(e.gesture.scale +','+e.gesture.scale +', camera.z = '+this.camera.position.z);
			this.update();
			return;
		}
		/*if( /(rotate)/i.test(e.type) ) {
			this.camera.position.z += ((-1+e.gesture.scale)*50);
			console.log(e.gesture.scale +', camera.z = '+this.camera.position.z);
			this.update();
			return;
		}*/
		var offset = this.$container.offset()
			,x = e.gesture.center.pageX - offset.left
			,y = e.gesture.center.pageY - offset.top
			,fullWidth = this.$container.find('canvas').width()
			,halfWidth = fullWidth/2
			,fullHeight = this.$container.find('canvas').height()
			,halfHeight = fullHeight/2
			,xynow = [(x-halfWidth)/halfWidth,-(y-halfHeight)/halfHeight]
			,vector = new THREE.Vector3( xynow[0], xynow[1], 1 );
		//console.log(x+','+y+' converted to '+xynow+' with '+halfWidth+' and '+halfHeight);
		this.projector.unprojectVector( vector, this.camera );
		this.raycaster.set( this.camera.position, vector.sub( this.camera.position ).normalize() );
		var a = this.raycaster.intersectObjects( this.scene.children );
		var fieldHit = (a && a.length) ? _.find(a, function(o) { return /field/i.test(o.object.name); }) : null;
		if( fieldHit ) {
			var helperVector = new THREE.Vector3().copy( fieldHit.point );
			fieldHit.object.worldToLocal(helperVector);
			//console.log('raycasting fieldhit at point '+helperVector.x+','+helperVector.y/*a[0].object.point*/);
			this.addInput(helperVector.x,helperVector.y);
		}
		if( /(touch|release)/i.test(e.type) )
			this.stateModel.set('hammering',(this.stateModel.get('hammering') || 0)+1);
		var state = this.stateModel.get('state');
		if( (/doubletap/i.test(e.type) || fieldHit) && this[state][e.type] )
			this[state][e.type].apply(this);

		this.update();

	};

	this.requestAnimFrame = function() {
		return window.requestAnimationFrame		||
			window.webkitRequestAnimationFrame	||
			window.mozRequestAnimationFrame		||
			window.oRequestAnimationFrame		||
			window.msRequestAnimationFrame		||
			function( callback ){
				window.setTimeout(callback,50);
			};
	};
	this.resize = function(){
		var w = this.$container.width()
			,h = this.$container.height();
		this.$container.find('canvas').css({'display':'block',"margin" : "0 auto"});
		this.camera.aspect = w/h;
		this.camera.updateProjectionMatrix();
		this.renderer.setSize(w,h);
		this.update();
	};

	_.bindAll(this, 'resize', 'handleHammer');
	Hammer.defaults.behavior.touchAction = 'pan-y';
	//$(this.renderer.domElement).attr('touch-action',"none");
	var holdTimeout = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ?
		300 : 500;

	Hammer(this.renderer.domElement,{holdThreshold:5,holdTimeout:holdTimeout}).on("drag touch release hold doubletap transform",this.handleHammer);
	var self = this;
	setInterval(function(){
		var state = self.stateModel.get('state');
		if( self[state].tick ) {
			if( self.nextTickEpoch ) {
				if( self.nextTickEpoch < Date.now() ) {
					self[state].tick.apply(self);
					delete self.nextTickEpoch;
				}
			} else self[state].tick.apply(self);
		}
	},70);

}

//if( y > halfHeight )
//	_.extend(pos,{top:'auto',height:'auto',bottom:(fullHeight-y-25)+'px'});
//else
//if( x > halfWidth )
//	_.extend(pos,{right:(fullWidth-x-25)+'px',left:'auto'});
//else

/*
var canvas = OffCanvasManager.getTextureForText(s);
var texture = new THREE.Texture(canvas);
texture.needsUpdate = true;
var textMesh = scene.getObjectByName('flashText');
if( !textMesh ){
	var material = new THREE.MeshBasicMaterial( {map: texture, side:THREE.DoubleSide } );
	material.transparent = true;
	textMesh = new THREE.Mesh(new THREE.PlaneGeometry(canvas.width,canvas.height),material);
	textMesh.name = 'flashText';
	textMesh.position.set(0,500,0);
	scene.add( textMesh );
}
textMesh.material.map = texture;
if( this.flashTextTimeoutId )
	clearTimeout(this.flashTextTimeoutId);
this.flashTextTimeoutId = setTimeout(function(){
	scene.remove(textMesh);
},ms);


*/