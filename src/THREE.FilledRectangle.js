THREE.FilledRectangle = function(posX, posY, posZ,w,h,color) {
	THREE.Object3D.call( this );
	this.type = 'FilledRectangle';
	this.ww = w;
	this.hh = h;
	this.fillColor = color;
	var shape = new THREE.Shape();
	shape.moveTo( 0,0 );
	shape.lineTo( 0, w );
	shape.lineTo( h, w );
	shape.lineTo( h, 0 );
	shape.lineTo( 0, 0 );
	var points = shape.createPointsGeometry();
	this.position.set( posX, posY, posZ );
	var line = new THREE.Line( points, new THREE.LineBasicMaterial( { color: color, linewidth: 1 } ) );
	this.add(line);
};
THREE.FilledRectangle.prototype = Object.create( THREE.Object3D.prototype );
THREE.FilledRectangle.prototype.constructor = THREE.FilledRectangle;

THREE.FilledRectangle.prototype.setFill = function(v){
	this.remove(this.getObjectByName('fill'));
	var shape = new THREE.Shape();
	shape.moveTo( 0,0 );
	shape.lineTo( 0, this.ww );
	shape.lineTo( this.hh*v, this.ww );
	shape.lineTo( this.hh*v, 0 );
	shape.lineTo( 0, 0 );
	var geometry = new THREE.ShapeGeometry( shape );
	var mesh = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { color: this.fillColor } ) );
	mesh.name = 'fill';
	this.add(mesh);
};