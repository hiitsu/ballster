$(function(){

	var canvasContainer = document.getElementById('canvas-container');
	if( !canvasContainer )
		return; // running tests

	var tacmap = new TacMap(canvasContainer,sketchId);
	window.tacmap = tacmap;
	tacmap.resize();
	document.ontouchmove = function(e){
		if(e.target && e.target.className.indexOf('slider')==-1)
			e.preventDefault();
	}
	setTimeout(function(){ tacmap.update(); },250)
	FastClick.attach(document.body);
	tacmap.resetTo(6);

});