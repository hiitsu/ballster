/**
 * Scene subclass that has few generic drawing feature that try recycle allocated 3D objects rather than creating new ones
 * everytime.
 *
 * @constructor
 */
THREE.BaseScene = function(){
	THREE.Scene.call( this );
	var z = 60;
	var roofLightsPositions = [[300,650,z],[-300,-650,z],[-300,650,z],[300,-650,z]];
	_.each(roofLightsPositions,function(a,i){
		var light = createPointLight(a[0],a[1],a[2],0xffffff,0,0,0);
		this.add(light);
	},this);
};

THREE.BaseScene.prototype = Object.create( THREE.Scene.prototype );
THREE.BaseScene.prototype.constructor = THREE.BaseScene;

THREE.BaseScene.prototype.lineWith = function(id,points,color){
	if( !points || !points.length || points.length <= 1 ) return;
	var o = this.getObjectByName(id,true);
	if( !o ) {
		var geometry = new THREE.Geometry();
		geometry.dynamic = true;
		geometry.vertices = Array(256);
		_.each(geometry.vertices,function(e,i,a){ a[i] = new THREE.Vector3(0,0,0); });
		var material = new THREE.LineBasicMaterial({
			color: (color || 0x0000ff)
		});
		o = new THREE.Line(geometry, material);
		o.position.set(0, 0, 1.5);
		o.name = id;
		this.add(o);
	}
	_.each(o.geometry.vertices,function(p,i,a){
		a[i].x = i < this.length ? this[i].x : this[this.length-1].x;
		a[i].y = i < this.length ? this[i].y : this[this.length-1].y;
	},points);
	o.needsUpdate = true;
	o.geometry.verticesNeedUpdate = true;
};

THREE.BaseScene.prototype.replace = function(obj){
	this.remove(this.getObjectByName(obj.name,true));
	this.add(obj);
};

THREE.BaseScene.prototype.focusRing = function(x,y){
	var id = 'focusRing';
	var o = this.getObjectByName(id,true);
	if( !o ) {
		var material = new THREE.MeshBasicMaterial( { opacity:0.5, transparent:true,color: 0xcdcdcd } )
			,geometry = new THREE.CircleGeometry( 22, 22 );
		o = new THREE.Mesh( geometry, material );
		o.name = id;
		o.position.z = 1.5;
		this.add(o);
	}
	o.position.x = x;
	o.position.y = y;
	o.needsUpdate = true;
};

THREE.BaseScene.prototype.toggle = function(name,visible) {
	this.traverse(function (o) {
		if( o.name == name )
			o.visible = visible;
	});
};

THREE.BaseScene.prototype.add = function() {
	THREE.Scene.prototype.add.apply( this,arguments );

	/*if( arguments[0] && /field/i.test(arguments[0].name) ) {
		var bbox = new THREE.BoundingBoxHelper( arguments[0], 0xff0000 );
		bbox.update();
		this.add( bbox );
	}*/
};


THREE.BaseScene.prototype.reset = function(){
	for ( var i = this.children.length - 1; i >= 0 ; i -- ) {
		var obj = this.children[i];
		if( !obj || obj instanceof THREE.BaseCamera || obj instanceof THREE.SpotLight ) continue;
		if( obj.geometry && obj.geometry.dispose ) obj.geometry.dispose();
		if( obj.material && obj.material.dispose ) obj.material.dispose();
		if( obj.deallocate ) obj.deallocate();
		if( obj.dispose ) obj.dispose();
		this.remove(obj);
	}
};
THREE.BaseScene.prototype.dispose = function(){
	for ( var i = this.children.length - 1; i >= 0 ; i -- ) {
		var obj = this.children[i];
		if( obj.geometry && obj.geometry.dispose ) obj.geometry.dispose();
		if( obj.material && obj.material.dispose ) obj.material.dispose();
		if( obj.deallocate ) obj.deallocate();
		if( obj.dispose ) obj.dispose();
		this.remove(obj);
	}
};

THREE.BaseScene.prototype.triggerEvent = function(ev) {
	this.traverse(function (o) {
		o.dispatchEvent(ev);
	});
};