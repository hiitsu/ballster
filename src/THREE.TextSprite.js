THREE.TextSprite = function(posX, posY, posZ){
	THREE.Sprite.call( this );
	this.position.set( posX, posY, posZ );
};

THREE.TextSprite.prototype = Object.create( THREE.Sprite.prototype );
THREE.TextSprite.prototype.constructor = THREE.TextSprite;

THREE.TextSprite.prototype.setText = function(s,color,size,milliseconds){
	var canvas = OffCanvasManager.get().getTextureForText(s,color,size);
	var texture = new THREE.Texture(canvas);
	texture.needsUpdate = true;
	var spriteMaterial = new THREE.SpriteMaterial({
		map: texture
		,useScreenCoordinates: false
	});
	this.material = spriteMaterial;
	//this.scale.set(100,50,1.0);
	if( this.timeoutId )
		clearTimeout(this.timeoutId);
	var self = this;
	if( milliseconds )
		this.timeoutId = setTimeout(function(){ self.visible = false; },milliseconds);
	this.visible = true;
};
