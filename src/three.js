function makeTextSprite( message,parameters )
{
	if ( parameters === undefined ) parameters = {};

	var fontface = parameters.hasOwnProperty("fontface") ?
		parameters["fontface"] : "Arial";

	var fontsize = parameters.hasOwnProperty("fontsize") ?
		parameters["fontsize"] : 18;

	var borderThickness = parameters.hasOwnProperty("borderThickness") ?
		parameters["borderThickness"] : 4;

	var borderColor = parameters.hasOwnProperty("borderColor") ?
		parameters["borderColor"] : { r:0, g:0, b:0, a:1.0 };

	var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?
		parameters["backgroundColor"] : { r:255, g:255, b:255, a:1.0 };

	//var spriteAlignment = parameters.hasOwnProperty("alignment") ?
	//	parameters["alignment"] : THREE.SpriteAlignment.topLeft;

	//var spriteAlignment = THREE.SpriteAlignment.topLeft;


	var canvas = document.createElement('canvas');
	var context = canvas.getContext('2d');
	context.font = "Bold " + fontsize + "px " + fontface;

	// get size data (height depends only on font size)
	var metrics = context.measureText( message );
	var textWidth = metrics.width;

	// background color
	context.fillStyle   = "rgba(" + backgroundColor.r + "," + backgroundColor.g + ","
		+ backgroundColor.b + "," + backgroundColor.a + ")";
	// border color
	context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + ","
		+ borderColor.b + "," + borderColor.a + ")";

	context.lineWidth = borderThickness;
	roundedRect(context, borderThickness/2, borderThickness/2, textWidth + borderThickness, fontsize * 1.4 + borderThickness, 6);
	// 1.4 is extra height factor for text below baseline: g,j,p,q.

	// text color
	context.fillStyle = "rgba(0, 0, 0, 1.0)";

	context.fillText( message, borderThickness, fontsize + borderThickness);

	// canvas contents will be used for a texture
	var texture = new THREE.Texture(canvas)
	texture.needsUpdate = true;

	var spriteMaterial = new THREE.SpriteMaterial(
		{ map: texture, useScreenCoordinates: false } );
	var sprite = new THREE.Sprite( spriteMaterial );
	sprite.scale.set(200,100,1);
	return sprite;
}

function box(sizeX, sizeY, sizeZ, material) {
	var g = new THREE.BoxGeometry(sizeX, sizeY, sizeZ);
	var m = new THREE.MeshPhongMaterial(material);
	var b = new THREE.Mesh(g, m);
	b.receiveShadow = true;
	b.castShadow = true;
	return b;
}
function tiledTexture(url, repeatX, repeatY) {
	var texture = THREE.ImageUtils.loadTexture(url);
	texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
	texture.repeat.set(repeatX, repeatY);
	return texture;
}
function createPointLight(posX, posY, posZ, color, targetPosX, targetPosY, targetPosZ) {
	var light = new THREE.SpotLight(color);
	light.target.position.set( targetPosX, targetPosY, targetPosZ);
	//light.shadowCameraNear	  = 0.01;
	//light.shadowCameraFar	  = 20;
	light.castShadow		  = true;
	light.shadowDarkness	  = 0.3;
	//light.shadowCameraVisible = true;  //enable to see light vectors
	light.intensity 		  = 2.4;
	light.position.x = posX;
	light.position.y = posY;
	light.position.z = posZ;
	return light;
}

/**
 * Creates area rectangle that has possibility fill it partially or fully
 * @param posX
 * @param posY
 * @param posZ
 * @param w
 * @param h
 * @param color
 * @constructor
 */
function areaRectangle(posX, posY, posZ,w,h,color,transparent,opacity){
	if ( w > 0 && h < 0 ) {
		posX = posX + w;
		w = -w;
	} else if ( w < 0 && h > 0 ) {
		posY = posY + h;
		h = -h;
	}
	var group = new THREE.Object3D(),
		geometry = new THREE.PlaneGeometry(w,h,1,1,1),
		plane = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial( { color: color } ) );
	transparent = typeof transparent === "undefined" ? false : transparent;
	opacity = typeof opacity === "undefined" ? 1 : opacity;
	group.position.set( posX+w/2, posY+h/2, posZ );
	plane.material.transparent = transparent;
	plane.material.opacity = opacity;
	group.add(plane);
	return group;
}
